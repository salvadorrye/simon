document.addEventListener('DOMContentLoaded', () => {
	const FPS = 30;
	const WINWIDTH = 640
	const WINHEIGHT = 480
	const FLASHSPEED = 500
	const FLASHDELAY = 200
	const BUTTONSIZE = 200
	const BUTTONGAPSIZE = 20
	const TIMEOUT = 4

	const WHITE = 'rgb(255, 255, 255)';
	const BLACK = 'rgb(0, 0, 0)';
	const BRIGHTRED = 'rgb(255, 0, 0)';
	const RED = 'rgb(155, 0, 0)';
	const BRIGHTGREEN = 'rgb(0, 255, 0)';
	const GREEN = 'rgb(0, 155, 0)';
	const BRIGHTBLUE = 'rgb(0, 0, 255)';
	const BLUE = 'rgb(0, 0, 155)';
	const BRIGHTYELLOW = 'rgb(255, 255, 0)';
	const YELLOW = 'rgb(155, 155, 0)';
	const DARKGRAY = 'rgb(40, 40, 40)';

	const RECTANGLES = [
		{
			x: 0,
			y: 0,
			color: YELLOW
		},
		{
			x: BUTTONGAPSIZE + BUTTONSIZE,
			y: 0,
			color: BLUE
		},
		{
			x: 0,
			y: BUTTONGAPSIZE + BUTTONSIZE,
			color: RED
		},
		{
			x: BUTTONGAPSIZE + BUTTONSIZE,
			y: BUTTONGAPSIZE + BUTTONSIZE,
			color: GREEN
		}
	];

	let canvas = document.getElementById('myCanvas');
	canvas.style.backgroundColor = "black";
	canvas.addEventListener('click', (e) => {
		const mousePos = {
			x: e.clientX - canvas.offsetLeft,
			y: e.clientY - canvas.offsetTop
		};
		// get pixel under cursor
                const pixel = ctx.getImageData(mousePos.x, mousePos.y, 1, 1).data;

                // create rgb color for that pixel
                const color = `rgb(${pixel[0]},${pixel[1]},${pixel[2]})`;
		alert('click on: ' + color);
	});

	let ctx = canvas.getContext('2d');

	RECTANGLES.forEach(rect => {
		ctx.beginPath();
		ctx.fillStyle = rect.color;
		ctx.fillRect(rect.x + BUTTONGAPSIZE, rect.y + BUTTONGAPSIZE, BUTTONSIZE, BUTTONSIZE);
	});
});
